import React from "react";

const Article = ({ imageUrl, title, desc }) => {
  return (
    <div className="article">
      <img src={imageUrl} alt={desc} />
      <h4>{title}</h4>
      <p>{desc}</p>
    </div>
  )
}

const PageBtn = ({ numPage, disabled, onClick, ...other }) => {
  return (
    <button
      {...other}
      disabled={disabled}
      className="pageBtn"
      onClick={onClick}
    >
      {numPage}
    </button>
  )
}

export const News = () => {
  const [news, setNews] = React.useState([]);
  const [page, setPage] = React.useState(1);
  const [totalPages, setTotalPages] = React.useState(0);

  React.useEffect(() => {
    fetch(`https://newsapi.org/v2/top-headlines?country=ru&category=science&page=${page}&apiKey=4ef6093568eb4336bb4fc3a895580a74`)
      .then(res => res.json())
      .then(res => {
        // console.log(res);
        setNews(res.articles || []);

        //обычно новостей 20 в одном запросе
        setTotalPages(Math.ceil(res.totalResults / 20));
      });
  }, [page])

  return (
    <div>
      <h1>Новости</h1>
      <div className="articles">
        {!news.length ? (
          "Загрузка..."
        ) : (
          news.map(article => (
            <Article
              imageUrl={article.urlToImage}
              title={article.title}
              desc={article.description}
              key={article.url}
            />
          )))}
      </div>
      <div className="navigation">
        {[...Array(totalPages).fill(0)].map((_, i) => (
          <PageBtn
            numPage={i + 1}
            key={i}
            disabled={page === i + 1}
            onClick={() => setPage(i + 1)}
          />
        ))}
      </div>
    </div>
  );
};
