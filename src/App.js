import { Routes, Route, Link } from "react-router-dom";
import { StartPage } from "./StartPage";
import React from "react";
import { News } from "./News";
import "./styles.css";

export const App = () => {
  return (
    <div className="App">
      <div className="header">
        <Link to="/">На главную</Link>
        <Link to="/news">Новости</Link>
      </div>
      <Routes>
        <Route path="/" element={<StartPage />} />
        <Route path="/news" element={<News />} />
      </Routes>
    </div>
  );
};
